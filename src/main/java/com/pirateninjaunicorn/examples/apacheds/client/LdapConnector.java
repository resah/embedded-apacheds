package com.pirateninjaunicorn.examples.apacheds.client;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class LdapConnector {

	private final String host;
	private final String port;

	public LdapConnector(final String host, final String port) {
		this.host = host;
		this.port = port;
	}

	public NamingEnumeration<SearchResult> searchDirectory(
			final String searchString) throws NamingException {

		LdapContext ldapCtx = createLdapContext();

		final SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		return ldapCtx.search("", "(uid=" + searchString + ")", searchControls);
	}

	protected LdapContext createLdapContext() throws NamingException {
		Hashtable<String, String> environment = new Hashtable<String, String>();
		environment.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		environment.put(Context.PROVIDER_URL, "ldap://" + host + ":" + port
				+ "/");

		LdapContext ldapCtx = new InitialLdapContext(environment,
				new Control[0]);
		return ldapCtx;
	}
}
