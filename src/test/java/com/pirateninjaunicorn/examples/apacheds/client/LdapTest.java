package com.pirateninjaunicorn.examples.apacheds.client;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.directory.server.annotations.CreateLdapServer;
import org.apache.directory.server.annotations.CreateTransport;
import org.apache.directory.server.core.annotations.ApplyLdifFiles;
import org.apache.directory.server.core.annotations.CreateDS;
import org.apache.directory.server.core.annotations.CreatePartition;
import org.apache.directory.server.core.integ.AbstractLdapTestUnit;
import org.apache.directory.server.core.integ.FrameworkRunner;
import org.apache.directory.server.integ.ServerIntegrationUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(FrameworkRunner.class)
@CreateDS(allowAnonAccess = true, name = "Test", partitions = { @CreatePartition(name = "Test", suffix = "o=TEST") })
@CreateLdapServer(transports = { @CreateTransport(protocol = "LDAP") })
public class LdapTest extends AbstractLdapTestUnit {

	private LdapConnector ldapConnector;

	@Before
	public void setUp() throws NamingException, Exception {

		final LdapContext ctx = ServerIntegrationUtils.getWiredContext(
				ldapServer, null);

		ldapConnector = spy(new LdapConnector("localhost", "389"));
		doReturn(ctx).when(ldapConnector).createLdapContext();
	}

	@ApplyLdifFiles("ldap-example-data.ldif")
	@Test
	public void testSearch() throws Exception {
		NamingEnumeration<SearchResult> results = ldapConnector
				.searchDirectory("admin");
		assertTrue(results.hasMore());

		SearchResult firstResultItem = results.next();
		System.out.println(firstResultItem.getName());
	}
}
